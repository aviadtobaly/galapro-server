// Galapro Test Server - Node


const express = require('express')
const app = express()

// Open up CORS since we use localhost developement
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', 'http://localhost:8100');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Access-Control-Allow-Credentials','true');
    next();
});


// Set up our socket.io on port 3001
const http = require('http').Server(app);
let io = require('socket.io').listen(http);
http.listen(3001);

// Set up our DB connection
const MongoClient = require('mongodb').MongoClient;
let db;


// Connect on startup
MongoClient.connect('mongodb://localhost:27017/', function (err, client) {

    if (err) throw err

    db = client.db('galadb');

    // Used this to insert a collection to the database
        //db.collection('redirects').insert({url:'http://www.google.com',redirect: 'http://www.yahoo.com'});

});



io.on('connection', (socket) => {
    var relayInterval;
    socket.on('redirect', function(url){

        db.collection('redirects').find({url:url}).toArray(function (err, result) {
            var urlMode = true;

            if(result.length == 0 || err) {
                io.emit('url',{error:true});
                clearInterval(relayInterval);
                return;
            };

            io.emit('url', {url: result[0].redirect});
            relayInterval = setInterval(function(){
                var data = result[0];
                var url = urlMode ? data.url : data.redirect;

                io.emit('url', {url: url});
                urlMode = !urlMode;
            },10000)

        });
    });


    // Clean up
    socket.on('end', function (){
        clearInterval(relayInterval);
    });
});


// Part 1
app.get('/api/redirect', (req, res) => {
    var resObj = {url:''};
    if(req.query.url && req.query.url.indexOf('google.com') > -1) {
        resObj.url = 'http://www.yahoo.com';

    }

    res.send(resObj)
})

app.listen(3000, () => console.log('GalaPro Server'))